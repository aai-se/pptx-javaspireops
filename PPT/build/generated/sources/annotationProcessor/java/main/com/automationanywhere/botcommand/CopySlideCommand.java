package com.automationanywhere.botcommand;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Double;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class CopySlideCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(CopySlideCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    CopySlide command = new CopySlide();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("inputFilePath") && parameters.get("inputFilePath") != null && parameters.get("inputFilePath").get() != null) {
      convertedParameters.put("inputFilePath", parameters.get("inputFilePath").get());
      if(convertedParameters.get("inputFilePath") !=null && !(convertedParameters.get("inputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","inputFilePath", "String", parameters.get("inputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("inputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","inputFilePath"));
    }
    if(convertedParameters.containsKey("inputFilePath")) {
      String filePath= ((String)convertedParameters.get("inputFilePath"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilePath","PPTX"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("PPTX".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilePath","PPTX"));
      }

    }
    if(parameters.containsKey("outputFilePath") && parameters.get("outputFilePath") != null && parameters.get("outputFilePath").get() != null) {
      convertedParameters.put("outputFilePath", parameters.get("outputFilePath").get());
      if(convertedParameters.get("outputFilePath") !=null && !(convertedParameters.get("outputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","outputFilePath", "String", parameters.get("outputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("outputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","outputFilePath"));
    }
    if(convertedParameters.containsKey("outputFilePath")) {
      String filePath= ((String)convertedParameters.get("outputFilePath"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","outputFilePath","PPTX"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("PPTX".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","outputFilePath","PPTX"));
      }

    }
    if(parameters.containsKey("copyPosition") && parameters.get("copyPosition") != null && parameters.get("copyPosition").get() != null) {
      convertedParameters.put("copyPosition", parameters.get("copyPosition").get());
      if(convertedParameters.get("copyPosition") !=null && !(convertedParameters.get("copyPosition") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","copyPosition", "Double", parameters.get("copyPosition").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("copyPosition") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","copyPosition"));
    }

    if(parameters.containsKey("copyMode") && parameters.get("copyMode") != null && parameters.get("copyMode").get() != null) {
      convertedParameters.put("copyMode", parameters.get("copyMode").get());
      if(convertedParameters.get("copyMode") !=null && !(convertedParameters.get("copyMode") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","copyMode", "String", parameters.get("copyMode").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("copyMode") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","copyMode"));
    }
    if(convertedParameters.get("copyMode") != null) {
      switch((String)convertedParameters.get("copyMode")) {
        case "Append" : {

        } break;
        case "Insert" : {
          if(parameters.containsKey("insertPosition") && parameters.get("insertPosition") != null && parameters.get("insertPosition").get() != null) {
            convertedParameters.put("insertPosition", parameters.get("insertPosition").get());
            if(convertedParameters.get("insertPosition") !=null && !(convertedParameters.get("insertPosition") instanceof Double)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","insertPosition", "Double", parameters.get("insertPosition").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("insertPosition") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","insertPosition"));
          }


        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","copyMode"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("inputFilePath"),(String)convertedParameters.get("outputFilePath"),(Double)convertedParameters.get("copyPosition"),(String)convertedParameters.get("copyMode"),(Double)convertedParameters.get("insertPosition")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
