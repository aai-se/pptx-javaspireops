package com.automationanywhere.botcommand;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Double;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AddListToPPTCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(AddListToPPTCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    AddListToPPT command = new AddListToPPT();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("inputFilepath") && parameters.get("inputFilepath") != null && parameters.get("inputFilepath").get() != null) {
      convertedParameters.put("inputFilepath", parameters.get("inputFilepath").get());
      if(convertedParameters.get("inputFilepath") !=null && !(convertedParameters.get("inputFilepath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","inputFilepath", "String", parameters.get("inputFilepath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("inputFilepath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","inputFilepath"));
    }
    if(convertedParameters.containsKey("inputFilepath")) {
      String filePath= ((String)convertedParameters.get("inputFilepath"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilepath","PPTX"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("PPTX".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilepath","PPTX"));
      }

    }
    if(parameters.containsKey("outputFilePath") && parameters.get("outputFilePath") != null && parameters.get("outputFilePath").get() != null) {
      convertedParameters.put("outputFilePath", parameters.get("outputFilePath").get());
      if(convertedParameters.get("outputFilePath") !=null && !(convertedParameters.get("outputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","outputFilePath", "String", parameters.get("outputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("outputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","outputFilePath"));
    }
    if(convertedParameters.containsKey("outputFilePath")) {
      String filePath= ((String)convertedParameters.get("outputFilePath"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","outputFilePath","PPTX"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("PPTX".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","outputFilePath","PPTX"));
      }

    }
    if(parameters.containsKey("slideNum") && parameters.get("slideNum") != null && parameters.get("slideNum").get() != null) {
      convertedParameters.put("slideNum", parameters.get("slideNum").get());
      if(convertedParameters.get("slideNum") !=null && !(convertedParameters.get("slideNum") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","slideNum", "Double", parameters.get("slideNum").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("slideNum") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","slideNum"));
    }

    if(parameters.containsKey("x") && parameters.get("x") != null && parameters.get("x").get() != null) {
      convertedParameters.put("x", parameters.get("x").get());
      if(convertedParameters.get("x") !=null && !(convertedParameters.get("x") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","x", "Double", parameters.get("x").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("x") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","x"));
    }

    if(parameters.containsKey("y") && parameters.get("y") != null && parameters.get("y").get() != null) {
      convertedParameters.put("y", parameters.get("y").get());
      if(convertedParameters.get("y") !=null && !(convertedParameters.get("y") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","y", "Double", parameters.get("y").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("y") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","y"));
    }

    if(parameters.containsKey("width") && parameters.get("width") != null && parameters.get("width").get() != null) {
      convertedParameters.put("width", parameters.get("width").get());
      if(convertedParameters.get("width") !=null && !(convertedParameters.get("width") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","width", "Double", parameters.get("width").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("width") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","width"));
    }

    if(parameters.containsKey("height") && parameters.get("height") != null && parameters.get("height").get() != null) {
      convertedParameters.put("height", parameters.get("height").get());
      if(convertedParameters.get("height") !=null && !(convertedParameters.get("height") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","height", "Double", parameters.get("height").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("height") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","height"));
    }

    if(parameters.containsKey("sourceList") && parameters.get("sourceList") != null && parameters.get("sourceList").get() != null) {
      convertedParameters.put("sourceList", parameters.get("sourceList").get());
      if(convertedParameters.get("sourceList") !=null && !(convertedParameters.get("sourceList") instanceof List)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sourceList", "List", parameters.get("sourceList").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sourceList") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sourceList"));
    }

    if(parameters.containsKey("format") && parameters.get("format") != null && parameters.get("format").get() != null) {
      convertedParameters.put("format", parameters.get("format").get());
      if(convertedParameters.get("format") !=null && !(convertedParameters.get("format") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","format", "String", parameters.get("format").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("format") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","format"));
    }
    if(convertedParameters.get("format") != null) {
      switch((String)convertedParameters.get("format")) {
        case "Bulleted" : {

        } break;
        case "Numbered" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","format"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("inputFilepath"),(String)convertedParameters.get("outputFilePath"),(Double)convertedParameters.get("slideNum"),(Double)convertedParameters.get("x"),(Double)convertedParameters.get("y"),(Double)convertedParameters.get("width"),(Double)convertedParameters.get("height"),(List<StringValue>)convertedParameters.get("sourceList"),(String)convertedParameters.get("format")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
