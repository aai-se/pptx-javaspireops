package com.automationanywhere.botcommand;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AddFooterCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(AddFooterCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    AddFooter command = new AddFooter();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("inputFilePath") && parameters.get("inputFilePath") != null && parameters.get("inputFilePath").get() != null) {
      convertedParameters.put("inputFilePath", parameters.get("inputFilePath").get());
      if(convertedParameters.get("inputFilePath") !=null && !(convertedParameters.get("inputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","inputFilePath", "String", parameters.get("inputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("inputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","inputFilePath"));
    }
    if(convertedParameters.containsKey("inputFilePath")) {
      String filePath= ((String)convertedParameters.get("inputFilePath"));
      int lastIndxDot = filePath.lastIndexOf(".");
      if (lastIndxDot == -1 || lastIndxDot >= filePath.length()) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilePath","PPTX"));
      }
      String fileExtension = filePath.substring(lastIndxDot + 1);
      if(!Arrays.stream("PPTX".split(",")).anyMatch(fileExtension::equalsIgnoreCase))  {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.FileExtension","inputFilePath","PPTX"));
      }

    }
    if(parameters.containsKey("addFooterText") && parameters.get("addFooterText") != null && parameters.get("addFooterText").get() != null) {
      convertedParameters.put("addFooterText", parameters.get("addFooterText").get());
      if(convertedParameters.get("addFooterText") !=null && !(convertedParameters.get("addFooterText") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","addFooterText", "Boolean", parameters.get("addFooterText").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("addFooterText") != null && (Boolean)convertedParameters.get("addFooterText")) {
      if(parameters.containsKey("footerText") && parameters.get("footerText") != null && parameters.get("footerText").get() != null) {
        convertedParameters.put("footerText", parameters.get("footerText").get());
        if(convertedParameters.get("footerText") !=null && !(convertedParameters.get("footerText") instanceof String)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","footerText", "String", parameters.get("footerText").get().getClass().getSimpleName()));
        }
      }
      if(convertedParameters.get("footerText") == null) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","footerText"));
      }

    }

    if(parameters.containsKey("addPageNum") && parameters.get("addPageNum") != null && parameters.get("addPageNum").get() != null) {
      convertedParameters.put("addPageNum", parameters.get("addPageNum").get());
      if(convertedParameters.get("addPageNum") !=null && !(convertedParameters.get("addPageNum") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","addPageNum", "Boolean", parameters.get("addPageNum").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("addDate") && parameters.get("addDate") != null && parameters.get("addDate").get() != null) {
      convertedParameters.put("addDate", parameters.get("addDate").get());
      if(convertedParameters.get("addDate") !=null && !(convertedParameters.get("addDate") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","addDate", "Boolean", parameters.get("addDate").get().getClass().getSimpleName()));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("inputFilePath"),(Boolean)convertedParameters.get("addFooterText"),(String)convertedParameters.get("footerText"),(Boolean)convertedParameters.get("addPageNum"),(Boolean)convertedParameters.get("addDate")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
