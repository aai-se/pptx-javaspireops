import com.automationanywhere.botcommand.*;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestingPPT {
    @Test
    public void testPPTReadCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\PPT\\PPT Package Testing.pptx");
        String filePathAsString = filePath.toString();
        ReadPPT ReadPPTFile = new ReadPPT();
        Value<String> Output = ReadPPTFile.action(filePathAsString);
        System.out.println(Output.get());
    }
    @Test
    public void testPPTAppendSlideCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        AppendSlide AppendSlideToPPTFile = new AppendSlide();
        Value<String> Output = AppendSlideToPPTFile.action(filePathAsString, filePathAsString);
        System.out.println(Output.get());
    }
    @Test
    public void testPPTRemoveSlideCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        RemoveSlide RemoveSlideFromPPTFile = new RemoveSlide();
        Value<String> Output = RemoveSlideFromPPTFile.action(filePathAsString, filePathAsString,2.0);
        System.out.println(Output.get());
    }
    @Test
    public void testPPTChangeSlideOrderCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        ChangeOrder ChangeOrderInPPT = new ChangeOrder();
        Value<String> Output = ChangeOrderInPPT.action(filePathAsString, filePathAsString,1.0,3.0);
        System.out.println(Output.get());
    }
    @Test
    public void testPPTAddListToPPTCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        AddListToPPT AddTextListToPPT = new AddListToPPT();
        List<StringValue> mylist = new ArrayList<StringValue>();
        mylist.add(new StringValue("Murali"));
        mylist.add(new StringValue("Krishna"));
        Value<String> Output = AddTextListToPPT.action(filePathAsString,filePathAsString,2.0,50.0,70.0,850.0,400.0,mylist,"Bulleted");
        System.out.println(Output);
    }
    @Test
    public void testPPTReplaceTextCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        ReplaceText ReplaceTextInPPT = new ReplaceText();
        Map map = new HashMap();
        //adding keys and values to the map
        map.put("Murali", "John Smith");
        map.put("Krishna", "Sedhu");
        Value<String> result = ReplaceTextInPPT.action(filePathAsString,filePathAsString,1.0,map);
        System.out.println(result.get());
    }
    @Test
    public void testPPTSplitSlideCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        Path filePath2  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Testnew.pptx");
        String filePathAsString = filePath.toString();
        String outputfilePathAsString = filePath2.toString();
        SplitSlide SplitSlideInPPT = new SplitSlide();
        Value<String> result = SplitSlideInPPT.action(filePathAsString,outputfilePathAsString,"Selected","2");
        System.out.println(result.get());
    }
    @Test
    public void testPPTGetTotalSlidesCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        String filePathAsString = filePath.toString();
        GetTotalSlides GetTotalSlidesInPPT = new GetTotalSlides();
        NumberValue result = GetTotalSlidesInPPT.action(filePathAsString);
        System.out.println(result.get().intValue());
    }
    @Test
    public void testPPTCopySlidesCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        Path filePath2  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test1.pptx");

        String filePathAsString = filePath.toString();
        String outputfilePathAsString = filePath2.toString();

        CopySlide CopySlideInPPT = new CopySlide();
        Value<String> result = CopySlideInPPT.action(filePathAsString,outputfilePathAsString,2.0,"Append",1.0);
        System.out.println(result.get());
    }
    @Test
    public void testPPTConvertCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test.pptx");
        Path filePath2  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\Test1.html");

        String filePathAsString = filePath.toString();
        String outputfilePathAsString = filePath2.toString();

        ConvertFile ConvertPPT = new ConvertFile();
        Value<String> result = ConvertPPT.action(filePathAsString,outputfilePathAsString);
        System.out.println(result.get());
    }
    @Test
    public void testPPTAddFooterCommand() {
        Path filePath  = Paths.get("C:\\Users\\murali.krishna\\OneDrive - Automation Anywhere Software Private Limited\\Documents\\A360.pptx");

        String filePathAsString = filePath.toString();

        AddFooter AddFooterPPT = new AddFooter();
        Value<String> result = AddFooterPPT.action(filePathAsString,true,"Test",false,false);
        System.out.println(result.get());
    }
}
