package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "ConvertFile", label = "[[ConvertFile.label]]",
        node_label = "[[ConvertFile.node_label]]", description = "[[ConvertFile.description]]", icon = "convert.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[ConvertFile.return_label]]", return_type = STRING, return_required = false, return_description = "[[ConvertFile.return_description]]")

public class ConvertFile {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[ConvertFile.filePath.label]]")
            @FileExtension("pptx,odp")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[ConvertFile.outputFilePath.label]]")
            @FileExtension("pdf,html,xps,odp,pptx")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String  outputFilePath) {

        //Business logic
        PPTOperations Conversion = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath,outputFilePath);
        Object result = Conversion.Convert();
        if (result instanceof Exception){
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception)result).getMessage()));
        }
        //Return StringValue.
        return new StringValue(result);
    }
}
