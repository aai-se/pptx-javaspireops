package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "AddFooter", label = "[[AddFooter.label]]",
        node_label = "[[AddFooter.node_label]]", description = "[[AddFooter.description]]", icon = "footer.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[AddFooter.return_label]]", return_type = STRING, return_required = true, return_description = "[[AddFooter.return_description]]")

public class AddFooter {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[AddFooter.filePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = CHECKBOX)
            //UI labels.
            @Pkg(label = "[[AddFooter.addFooterText.label]]")
                    Boolean addFooterText,
            @Idx(index = "2.1", type = TEXT)
            //UI labels.
            @Pkg(label = "[[AddFooter.footerText.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String footerText,
            @Idx(index = "3", type = CHECKBOX)
            //UI labels.
            @Pkg(label = "[[AddFooter.addPageNum.label]]")
            //Ensure that a validation error is thrown when the value is null.
                    Boolean addPageNum,
            @Idx(index = "4", type = CHECKBOX)
            //UI labels.
            @Pkg(label = "[[AddFooter.addDate.label]]")
            //Ensure that a validation error is thrown when the value is null.
                    Boolean addDate) {

        //Business logic
        PPTOperations addFooter = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath);
        String result = addFooter.AddFooter(footerText,addFooterText,addPageNum,addDate);
        //Return StringValue.
        return new StringValue(result);
    }
}