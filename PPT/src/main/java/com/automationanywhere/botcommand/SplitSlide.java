package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "SplitSlide", label = "[[SplitSlide.label]]",
        node_label = "[[SplitSlide.node_label]]", description = "[[SplitSlide.description]]", icon = "split.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[SplitSlide.return_label]]", return_type = STRING, return_required = false, return_description = "[[SplitSlide.return_description]]")

public class SplitSlide {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[SplitSlide.inputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[SplitSlide.outputFilePath.label]]", description = "[[SplitSlide.outputFilePath.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = RADIO,
                    //Providing the multiple options under the RADIO.
                    options = {
                            @Idx.Option(index = "3.1", pkg = @Pkg(node_label = "[[SplitSlide.All.node_label]]", label = "[[SplitSlide.All.label]]", value = "All")),
                            @Idx.Option(index = "3.2", pkg = @Pkg(node_label = "[[SplitSlide.Selected.node_label]]", label = "[[SplitSlide.Selected.label]]", value = "Selected"))
                    })
            @Pkg(label = "[[SplitSlide.slideCount.label]]", default_value = "All", default_value_type = DataType.STRING)
                    String slideCount,
            @Idx(index = "3.2.1", type = TEXT)
            //UI labels.
            @Pkg(label = "[[SplitSlide.slidePosition.label]]", description = "[[SplitSlide.slidePosition.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String  slidePosition) {

        
        //Business logic
        PPTOperations SplitSlide_ToPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath,outputFilePath);
        Object result = SplitSlide_ToPPT.SplitSlide(slidePosition,slideCount);
        if (result instanceof Exception)
            //Return Exception
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception) result).getMessage()));
        else
            //Return StringValue.
            return new StringValue(result);
    }
}
