package com.automationanywhere.botcommand;


import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "ChangeOrder", label = "[[ChangeOrder.label]]",
        node_label = "[[ChangeOrder.node_label]]", description = "[[ChangeOrder.description]]", icon = "changeOrder.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[ChangeOrder.return_label]]", return_type = STRING, return_required = false, return_description = "[[ChangeOrder.return_description]]")

public class ChangeOrder {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[ChangeOrder.filePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilepath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[ChangeOrder.outputFilePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[ChangeOrder.from.label]]",description = "[[ChangeOrder.from.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double from,
            @Idx(index = "4", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[ChangeOrder.to.label]]",description = "[[ChangeOrder.to.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double to) {

        //Internal validation. No null check needed as we have NotEmpty property.
        if (from <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Current Slide Position"));
        else if (to <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "New Slide Position"));

        //Business logic
        from = from - 1;
        PPTOperations ChangeSlideOrder = PPTLoaderClass.getInstance().getPPTInstance(inputFilepath,outputFilePath);
        Object result = ChangeSlideOrder.ChangeSlideOrder(from,to);

        //Throw error, if the given slide position is greater than the total slides in PPT
        if (result instanceof Exception)
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception) result).getMessage()));

        //Return StringValue.
        return new StringValue(result);
    }
}
