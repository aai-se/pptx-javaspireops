package com.automationanywhere.botcommand.PPT_Loader;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.businesslogic.PPTOperationsUsingSpire;

public final class PPTLoaderClass {

    private static class SingletonInstance {
        private static final PPTLoaderClass INSTANCE = new PPTLoaderClass();
    }

    public static PPTLoaderClass getInstance(){
        return SingletonInstance.INSTANCE;
    }

    public PPTOperations getPPTInstance(String inputFilePath){
        return new PPTOperationsUsingSpire(inputFilePath);
    }
    public PPTOperations getPPTInstance(String inputFilePath, String outputFilePath){
        return new PPTOperationsUsingSpire(inputFilePath,outputFilePath);
    }
}
