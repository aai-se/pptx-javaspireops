package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "ReadPPT", label = "[[ReadPPT.label]]",
        node_label = "[[ReadPPT.node_label]]", description = "[[ReadPPT.description]]", icon = "read.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[ReadPPT.return_label]]", return_type = STRING, return_required = true, return_description = "[[ReadPPT.return_description]]")

public class ReadPPT {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[ReadPPT.filePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilePath) {

        //Business logic
        PPTOperations ReadPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath);
        String result = ReadPPT.ReadPPT();
        //Return StringValue.
        return new StringValue(result);
    }
}