package com.automationanywhere.botcommand.PPT_Interface;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.core.security.SecureString;

import java.util.List;
import java.util.Map;

public interface PPTOperations {
    String AddListWithFormatting(Double slideNum, List<StringValue> inputList,
                                 Double x, Double y, Double width, Double height, String format);
    String AppendSlides();
    Object ChangeSlideOrder(Double currentSlidePos, Double posToMove);
    Object Convert();
    String ReadPPT();
    Object RemoveSlideFromPPT(Double index);
    Object ReplaceText(Double slideNumber, Map<String, Value> inputMap);
    Object SplitSlide(String slideNumber, String slideCount);
    Object getTotalSlides();
    String EncryptOrDecrypt(String EncryptOrDecryptSelection, SecureString password);
    Object CopySlide(String copyMode, Double copyPosition, Double insertPosition);
    String AddFooter(String footerText, Boolean addFooterText, Boolean addPageNum, Boolean addDateTime);
}
