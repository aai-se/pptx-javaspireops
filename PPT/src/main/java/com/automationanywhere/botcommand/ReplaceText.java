package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


@BotCommand
@CommandPkg(label = "[[ReplaceText.label]]",
        description = "[[ReplaceText.description]]", icon = "replace.svg", name = "ReplaceText",node_label = "[[ReplaceText.node_label]]",
        return_label = "[[ReplaceText.return_label]]", return_description = "[[ReplaceText.return_description]]", return_type = STRING, return_required = false)
public class ReplaceText {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[ReplaceText.inputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilepath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[ReplaceText.outputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[ReplaceText.slidePosition.label]]",description = "[[ReplaceText.slidePosition.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double slidePos,
            @Idx(index = "4", type = AttributeType.DICTIONARY)
            @Pkg(label = "[[ReplaceText.inputDict.label]]", description = "[[ReplaceText.inputDict.description]]")
            @NotEmpty
                    Map<String, Value> keyValuePair) {

        //Internal validation. No null check needed as we have NotEmpty property.
        if (slidePos <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Slide Position"));

        //Business Logic
        PPTOperations replaceText = PPTLoaderClass.getInstance().getPPTInstance(inputFilepath,outputFilePath);
        slidePos=slidePos-1;
        Object result = replaceText.ReplaceText(slidePos,keyValuePair);

        //Throw error, if the given slide position is greater than the total slides in PPT
        if (result instanceof Exception)
            throw new BotCommandException(MESSAGES.getString("customMessage",((Exception) result).getMessage()));
        return new StringValue(result);
    }
}