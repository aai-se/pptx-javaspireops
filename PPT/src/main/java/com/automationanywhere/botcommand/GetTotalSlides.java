package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.NUMBER;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "GetTotalSlides", label = "[[GetTotalSlides.label]]",
        node_label = "[[GetTotalSlides.node_label]]", description = "[[GetTotalSlides.description]]", icon = "total.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[GetTotalSlides.return_label]]", return_type = NUMBER, return_required = true, return_description = "[[GetTotalSlides.return_description]]")

public class GetTotalSlides {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public NumberValue action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[GetTotalSlides.filePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilePath) {

        //Business logic
        PPTOperations getTotalSlides = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath);
        Object result = getTotalSlides.getTotalSlides();
        if (result instanceof Exception){
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception) result).getMessage()));
        }
        //Return StringValue.
        return new NumberValue(result);
    }
}