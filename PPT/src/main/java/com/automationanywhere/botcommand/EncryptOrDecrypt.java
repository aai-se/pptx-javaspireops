package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.CredentialAllowPassword;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "EncryptOrDecrypt", label = "[[EncryptOrDecrypt.label]]",
        node_label = "[[EncryptOrDecrypt.node_label]]", description = "[[EncryptOrDecrypt.description]]", icon = "encryption.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[EncryptOrDecrypt.return_label]]", return_type = STRING, return_required = false, return_description = "[[EncryptOrDecrypt.return_description]]")

public class EncryptOrDecrypt {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[EncryptOrDecrypt.inputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = RADIO,
                    //Providing the multiple options under the RADIO.
                    options = {
                            @Idx.Option(index = "2.1", pkg = @Pkg(node_label = "[[EncryptOrDecrypt.Encrypt.node_label]]", label = "[[EncryptOrDecrypt.Encrypt.label]]", value = "Encrypt")),
                            @Idx.Option(index = "2.2", pkg = @Pkg(node_label = "[[EncryptOrDecrypt.Decrypt.node_label]]", label = "[[EncryptOrDecrypt.Decrypt.label]]", value = "Decrypt"))
                    })
            @Pkg(label = "[[EncryptOrDecrypt.EncryptOrDecryptSelection.label]]", default_value = "Encrypt", default_value_type = DataType.STRING)
                    String EncryptOrDecryptSelection,
            @Idx(index = "3", type = CREDENTIAL)
            //UI labels.
            @Pkg(label = "[[EncryptOrDecrypt.password.label]]", description = "[[EncryptOrDecrypt.password.description]]")
            @CredentialAllowPassword
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    SecureString password) {

        //Business logic
        PPTOperations EncryptOrDecryptPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath);
        String result = EncryptOrDecryptPPT.EncryptOrDecrypt(EncryptOrDecryptSelection,password);
        //Return StringValue.
        return new StringValue(result);
    }
}
