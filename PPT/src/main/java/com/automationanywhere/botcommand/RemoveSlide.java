package com.automationanywhere.botcommand;


import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "RemoveSlide", label = "[[RemoveSlide.label]]",
        node_label = "[[RemoveSlide.node_label]]", description = "[[RemoveSlide.description]]", icon = "remove.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[RemoveSlide.return_label]]", return_type = STRING, return_required = false, return_description = "[[RemoveSlide.return_description]]")

public class RemoveSlide {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[RemoveSlide.filePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String inputFilepath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[RemoveSlide.outputFilePath.label]]")
            @FileExtension("PPTX")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[RemoveSlide.index.label]]",description = "[[RemoveSlide.index.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                Double index) {

        //Internal validation. No null check needed as we have NotEmpty.
        if (index <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Slide Index"));

        //Business logic
        index = index-1;
        PPTOperations RemoveSlide_FromPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilepath,outputFilePath);
        Object result = RemoveSlide_FromPPT.RemoveSlideFromPPT(index);

        //Throw error, if the given slide position is greater than the total slides in PPT
        if (result instanceof Exception)
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception)result).getMessage()));

        //Return StringValue.
        return new StringValue(result);
    }
}
