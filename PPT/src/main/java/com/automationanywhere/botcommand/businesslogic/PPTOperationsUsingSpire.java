package com.automationanywhere.botcommand.businesslogic;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.core.security.SecureString;
import com.spire.presentation.*;
import com.spire.presentation.diagrams.ISmartArt;
import com.spire.presentation.diagrams.ISmartArtNode;
import com.spire.presentation.drawing.FillFormatType;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PPTOperationsUsingSpire implements PPTOperations {
    private String inputFilePath;
    private String outputFilePath;

    public PPTOperationsUsingSpire(String inputFilePath){
        this.inputFilePath = inputFilePath;
    }
    public PPTOperationsUsingSpire(String inputFilePath, String outputFilePath){
        this(inputFilePath);
        this.outputFilePath = outputFilePath;
    }

    @Override
    public String AddListWithFormatting(Double slideNum, List<StringValue> inputList, Double x, Double y, Double width, Double height, String format) {
        try {
            //Create a Presentation instance
            Presentation ppt = loadPPT(this.inputFilePath);
//            Presentation ppt = new Presentation();
//            ppt.loadFromFile(inputFilePath);
            //Get the slide from given index
            ISlide slide = ppt.getSlides().get(slideNum.intValue());

            Rectangle2D rect = new Rectangle2D.Double(x.intValue(), y.intValue(), width.intValue(), height.intValue());
            //Append a shape to the slide and set shape style
            IAutoShape shape = slide.getShapes().appendShape(ShapeType.RECTANGLE, rect);
            shape.getShapeStyle().getLineColor().setColor(Color.white);
            shape.getFill().setFillType(FillFormatType.NONE);
            shape.getTextFrame().setAutofitType(TextAutofitType.NORMAL);
            shape.getTextFrame().setAnchoringType(TextAnchorType.TOP);

            //Clear the default paragraph
            shape.getTextFrame().getParagraphs().clear();

            //Add a bulleted list
            for (int i = 0; i < inputList.size(); i++) {
                ParagraphEx paragraph = new ParagraphEx();
                shape.getTextFrame().getParagraphs().append(paragraph);
                paragraph.setText(inputList.get(i).toString());
                paragraph.getTextRanges().get(0).getFill().setFillType(FillFormatType.SOLID);
                paragraph.getTextRanges().get(0).getFill().getSolidColor().setColor(Color.black);
                switch (format) {
                    case ("Bulleted"): {
                        paragraph.setBulletType(TextBulletType.SYMBOL);
                        break;
                    }
                    case ("Numbered"): {
                        paragraph.setBulletType(TextBulletType.NUMBERED);
                        paragraph.setBulletStyle(NumberedBulletStyle.BULLET_ROMAN_LC_PERIOD);
                        break;
                    }
                }
            }
            //Save the document
            ppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        } catch (Exception e) {
            return (e.getMessage());
        }
    }

    @Override
    public String AppendSlides() {
        try {
            //Create a PPT document and load file
            Presentation ppt = loadPPT(this.inputFilePath);
//            Presentation presentation = new Presentation();
//            presentation.loadFromFile(filePath);
            //add new slide at the end of the document
            ppt.getSlides().append();
            //Save the document
            ppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public Object ChangeSlideOrder(Double currentSlidePos, Double posToMove) {
        try {
            Presentation ppt = loadPPT(this.inputFilePath);
//            Presentation presentation = new Presentation();
//            presentation.loadFromFile(inputFilePath);

            //Validation check for the slide positions greater than the total slides in PPT
            int totalSlides = ppt.getSlides().size() - 1;
            if (totalSlides < (posToMove.intValue()-1) || totalSlides < currentSlidePos.intValue())
                return new Exception("Slide position cannot be greater than the total slides in PPT");

            //Change Order Of the Slide
            ISlide slide = ppt.getSlides().get(currentSlidePos.intValue());
            slide.setSlideNumber(posToMove.intValue());

            //Save the document
            ppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public Object Convert() {
        try {
            String inputFileExtension = getExtension(this.inputFilePath);
            String outputFileExtension = getExtension(this.outputFilePath);
            if (inputFileExtension.isBlank() || outputFileExtension.isBlank()){
                return new Exception("One of the file extension is blank. Please check the filepath before continuing further");
            }
            else if (inputFileExtension.equals(outputFileExtension)){
                return new Exception("The source file is having the same extension as the destination file");
            }
            //Create a PPT document and load file
            Presentation ppt = loadPPT(this.inputFilePath);
            FileFormat fileformat = null;
            switch (outputFileExtension){
                case("PDF"):
                    fileformat = FileFormat.PDF;
                    break;
                case("HTML"):
                    fileformat = FileFormat.HTML;
                    break;
                case("XPS"):
                    fileformat = FileFormat.XPS;
                    break;
                case("ODP"):
                    fileformat = FileFormat.ODP;
                    break;
                case("PPTX"):
                    fileformat = FileFormat.PPTX_2016;
                    break;
                default:
                    return new Exception("The output file format is null. Please check the destination file path");
            }
            //Save the document and convert and save as a PDF
            ppt.saveToFile(this.outputFilePath, fileformat);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public String ReadPPT() {
        try {
            //Create a Presentation instance
            Presentation ppt = loadPPT(this.inputFilePath);
            StringBuilder buffer = new StringBuilder();
            int slideCount = 1;
            //Loop through the slides in the document and extract text
            for (Object slide : ppt.getSlides()) {
                String page = "Slide: "+slideCount+"\n";
                buffer.append(page);
                for (Object shape : ((ISlide) slide).getShapes()) {
                    if (shape instanceof IAutoShape) {
                        for (Object tp : ((IAutoShape) shape).getTextFrame().getParagraphs()) {
                            buffer.append(((ParagraphEx) tp).getText().trim());
                            buffer.append("\n");
                        }
                        buffer.append("\n");
                    }
                    else if (shape instanceof ISmartArt) {
                        for (Object node : ((ISmartArt) shape).getNodes()) {
                            buffer.append(((ISmartArtNode)node).getTextFrame().getText().trim());
                            buffer.append("\n");
                        }
                        buffer.append("\n");
                    }
                    else if (shape instanceof ITable) {
                        ITable table = (ITable) shape;
                        for (int i = 0; i < table.getTableRows().size(); i++) {
                            for (int j = 0; j < table.getColumnsList().size(); j++) {
                                buffer.append(table.get(j,i).getTextFrame().getText());
                                buffer.append("\t");
                            }
                            buffer.append("\n");
                        }
                        buffer.append("\n");
                    }
                }
                slideCount += 1;
                buffer.append("\n\n");
            }
            //return text
            return buffer.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public Object RemoveSlideFromPPT(Double index) {
        try {
            //Load PPT file
            Presentation ppt = loadPPT(this.inputFilePath);
//            Presentation presentation = new Presentation();
//            presentation.loadFromFile(inputFilePath);

            //Validation check for the slide positions greater than the total slides in PPT
            int totalSlides = ppt.getSlides().size() - 1;
            if (totalSlides < index.intValue())
                return new Exception("Slide position cannot be greater than the total slides in PPT");

            //Remove slide present in the given index
            ppt.getSlides().removeAt(index.intValue());

            //Save the document
            ppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public Object ReplaceText(Double slideNumber, Map<String, Value> inputMap) {
        try {
            //create a Presentation object
            Presentation ppt = loadPPT(this.inputFilePath);
//            Presentation presentation = new Presentation();
//            presentation.loadFromFile(inputFilePath);

            //Validation check for the slide positions greater than the total slides in PPT
            int totalSlides = ppt.getSlides().size() - 1;
            if (totalSlides < slideNumber.intValue())
                return new Exception("Slide position cannot be greater than the total slides in PPT");

            //get the slide from given position
            ISlide slide = ppt.getSlides().get(slideNumber.intValue());

            //replace text in the slide
            for (Object shape : slide.getShapes()) {
                if (shape instanceof IAutoShape) {
                    for (Object paragraph : ((IAutoShape) shape).getTextFrame().getParagraphs()) {
                        ParagraphEx paragraphEx = (ParagraphEx) paragraph;
                        for (String key : inputMap.keySet()) {
                            if (paragraphEx.getText().contains(key)) {
                                Value value = inputMap.get(key);
                                String val = value.get().toString();
                                paragraphEx.setText(paragraphEx.getText().replace(key, val));
                            }
                        }
                    }
                }
            }
            //save to file
            ppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public Object SplitSlide(String slideNumber, String slideCount) {
        try {
            //create a Presentation object
            Presentation ppt = loadPPT(this.inputFilePath);
            //Validation check for the slide positions greater than the total slides in PPT
            int totalSlides = ppt.getSlides().size() - 1;
            int startingSlideNum = 0, endingSlideNum = 0;
            if (slideCount.equals("Selected")) {
                Pattern p = Pattern.compile("[0-9]+(-[0-9]+)?");
                Matcher m = p.matcher(slideNumber);
                if (m.matches()) {
                    if (slideNumber.contains("-")) {
                        String[] slides = slideNumber.split("-");
                        startingSlideNum = Integer.parseInt(slides[0]);
                        endingSlideNum = Integer.parseInt(slides[1]);
                        startingSlideNum -= 1;
                        endingSlideNum -= 1;
                        if (totalSlides < startingSlideNum || totalSlides < endingSlideNum)
                            return new Exception("Slide position cannot be greater than the total slides in PPT");
                        else if (startingSlideNum > endingSlideNum) {
                            return new Exception("Starting slide number is greater than the ending slide number");
                        }
                    } else {
                        startingSlideNum = Integer.parseInt(slideNumber);
                        endingSlideNum = startingSlideNum;
                        startingSlideNum -= 1;
                        endingSlideNum -= 1;
                        if (totalSlides < startingSlideNum)
                            return new Exception("Slide position cannot be greater than the total slides in PPT");
                    }
                } else {
                    return new Exception("Please check the slide position mentioned in the properties");
                }
                if (startingSlideNum < 0 || endingSlideNum < 0)
                    return new Exception("Slide position cannot be less than or equals zero");
                Presentation newppt = new Presentation();

                //Remove the default slide
                newppt.getSlides().removeAt(0);

                //Loop through the slides
                for (int i = startingSlideNum; i <= endingSlideNum; i++)
                    //Select a slide from the source file and append it to the new document
                    newppt.getSlides().append(ppt.getSlides().get(i));

                //Save to file
                newppt.saveToFile(this.outputFilePath, FileFormat.PPTX_2016);
            }
            else {
                endingSlideNum = totalSlides;
                outputFilePath = this.outputFilePath.replace(".pptx","");
                //Loop through the slides
                for (int i = 0; i <= endingSlideNum; i++)
                {
                    //Create an instance of Presentation class
                    Presentation newppt = new Presentation();

                    //Remove the default slide
                    newppt.getSlides().removeAt(0);

                    //Select a slide from the source file and append it to the new document
                    newppt.getSlides().append(ppt.getSlides().get(i));

                    //Save to file
                    newppt.saveToFile(String.format(this.outputFilePath+"_%d.pptx",i), FileFormat.PPTX_2016);
                }
            }
            //Create an instance of Presentation class

            return "Success";
        }
        catch (Exception e) {
            return e.getMessage();
        }
    }
    @Override
    public Object getTotalSlides() {
        try {
            return loadPPT(this.inputFilePath).getSlides().size();
        }
        catch(Exception e){
            return e;
        }
    }

    @Override
    public String EncryptOrDecrypt(String EncryptOrDecryptSelection, SecureString password) {
        try{
            Presentation ppt;
            if (EncryptOrDecryptSelection.equals("Encrypt")) {
                ppt = loadPPT(this.inputFilePath);
                //Encrypt PPT
                ppt.encrypt(password.getInsecureString());
            }
            else{
                ppt = loadEncryptedPPT(this.inputFilePath,password);
                //Remove encryption
                ppt.removeEncryption();
            }
            ppt.saveToFile(this.inputFilePath, FileFormat.PPTX_2016);
            return "Success";
        }
        catch(Exception e){
            return e.getMessage();
        }
    }

    @Override
    public Object CopySlide(String copyMode, Double copyPosition, Double insertPosition) {
        try {
            Presentation firstPPT = loadPPT(this.inputFilePath);
            int totalSlidesInFirstPPT = firstPPT.getSlides().size() - 1;
            if (totalSlidesInFirstPPT < copyPosition.intValue())
                return new Exception("Slide position cannot be greater than the total slides in PPT");

            Presentation secondPPT = loadPPT(this.outputFilePath);

            if (copyMode.equals("Insert")) {
                int totalSlidesInSecondPPT = secondPPT.getSlides().size();
                if (totalSlidesInSecondPPT < insertPosition.intValue())
                    return new Exception("Destination slide position cannot be greater than one slide in addition to the total slides in PPT");
                //Insert the specific slide from document one into the specified position of document two
                secondPPT.getSlides().insert(insertPosition.intValue(), firstPPT.getSlides().get(copyPosition.intValue()));
            }
            else {
                //Append the specific slide of document one to the end of document two
                secondPPT.getSlides().append(firstPPT.getSlides().get(copyPosition.intValue()));
            }

            //Save the document two to another file
            secondPPT.saveToFile(outputFilePath, FileFormat.PPTX_2016);
            return "Success";
        }
        catch(Exception e){
            return e.getMessage();
        }
    }

    @Override
    public String AddFooter(String footerText, Boolean addFooterText, Boolean addPageNum, Boolean addDateTime) {
        try {
            Presentation ppt = loadPPT(this.inputFilePath);
            //Add footer
            if (addFooterText) {
                ppt.setFooterText(footerText);
                ppt.setFooterVisible(true);
            }
            if (addPageNum) {
                //Set the page number visible
                ppt.setSlideNumberVisible(true);
            }
            if (addDateTime) {
                //Set the date visible
                ppt.setDateTimeVisible(true);
            }
            //Save the document
            ppt.saveToFile(this.inputFilePath, FileFormat.PPTX_2016);
            return "Success";
        }
        catch(Exception e){
            return e.getMessage();
        }
    }

    private Presentation loadPPT(String filePath) throws Exception {
            Presentation ppt = new Presentation();
            ppt.loadFromFile(filePath);
            return ppt;
    }
    private Presentation loadEncryptedPPT(String filePath, SecureString password) throws Exception {
        Presentation ppt = new Presentation();
        ppt.loadFromFile(filePath, FileFormat.PPTX_2016,password.getInsecureString());
        return ppt;
    }
    private String getExtension(String filepath){
        String fileExtension = "";
        Path filePathAsPath = Paths.get(filepath);
        String filename = filePathAsPath.getFileName().toString();
        int index = filename.lastIndexOf('.');
        if (index > 0) {
            fileExtension = filename.substring(index + 1).toUpperCase();
        }
        return fileExtension;
    }
}
