package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "AppendSlide", label = "[[AppendSlide.label]]",
        node_label = "[[AppendSlide.node_label]]", description = "[[AppendSlide.description]]", icon = "append.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[AppendSlide.return_label]]", return_type = STRING, return_required = false, return_description = "[[AppendSlide.return_description]]")

public class AppendSlide {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[AppendSlide.filePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[AppendSlide.outputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String  outputFilePath) {

        //Business logic
        PPTOperations AppendSlide_ToPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath,outputFilePath);
        String result = AppendSlide_ToPPT.AppendSlides();
        //Return StringValue.
        return new StringValue(result);
    }
}
