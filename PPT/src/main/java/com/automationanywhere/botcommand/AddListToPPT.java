package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.*;

@BotCommand

@CommandPkg(label = "[[AddList.label]]",
        description = "[[AddList.description]]", icon = "list.svg", name = "AddList", node_label = "[[AddList.node_label]]",
        return_label =  "[[AddList.return_label]]", return_description = "[[AddList.return_description]]", return_type = DataType.STRING, return_required = false)
public class AddListToPPT {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[AddList.filePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilepath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[AddList.outputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[AddList.slideNum.label]]",description = "[[AddList.slideNum.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double slideNum,
            @Idx(index = "4", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[AddList.x.label]]",description = "[[AddList.x.description]]",default_value = "50",default_value_type = DataType.NUMBER)
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double x,
            @Idx(index = "5", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[AddList.y.label]]",description = "[[AddList.y.description]]",default_value = "70",default_value_type = DataType.NUMBER)
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double y,
            @Idx(index = "6", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[AddList.width.label]]",description = "[[AddList.width.description]]",default_value = "850",default_value_type = DataType.NUMBER)
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double width,
            @Idx(index = "7", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[AddList.height.label]]",description = "[[AddList.height.description]]",default_value = "400",default_value_type = DataType.NUMBER)
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double height,
            @Idx(index = "8", type = AttributeType.LIST)
            @Pkg(label = "[[AddList.sourceList.label]]",description = "[[AddList.sourceList.description]]")
            @NotEmpty
                    List<StringValue> sourceList,
            @Idx(index = "9", type = RADIO,
                    //Providing the multiple options under the RADIO.
                    options = {
                            @Idx.Option(index = "9.1", pkg = @Pkg(node_label = "[[AddList.Bulleted.node_label]]", label = "[[AddList.Bulleted.label]]", value = "Bulleted")),
                            @Idx.Option(index = "9.2", pkg = @Pkg(node_label = "[[AddList.Numbered.node_label]]", label = "[[AddList.Numbered.label]]", value = "Numbered"))
                    })
            @Pkg(label = "[[AddList.Format.label]]", default_value = "Bulleted", default_value_type = DataType.STRING)
                    String format) {
        //Throw error, if the number fields are less than or equals zero.
        if (slideNum <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Slide Position"));
        else if (x <= 0 )
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Shape - X Axis"));
        else if (y <= 0 )
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Shape - Y Axis"));
        else if (width <= 0 )
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Shape - Width"));
        else if (height <= 0 )
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Shape - Height"));

        //Business Logic
        PPTOperations AddList = PPTLoaderClass.getInstance().getPPTInstance(inputFilepath,outputFilePath);
        slideNum = slideNum-1;
        String output =  AddList.AddListWithFormatting(slideNum,sourceList,x,y,width,height,format);
        return new StringValue(output);
    }
}
