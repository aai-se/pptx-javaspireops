package com.automationanywhere.botcommand;

import com.automationanywhere.botcommand.PPT_Interface.PPTOperations;
import com.automationanywhere.botcommand.PPT_Loader.PPTLoaderClass;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "CopySlide", label = "[[CopySlide.label]]",
        node_label = "[[CopySlide.node_label]]", description = "[[CopySlide.description]]", icon = "copy.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[CopySlide.return_label]]", return_type = STRING, return_required = false, return_description = "[[CopySlide.return_description]]")

public class CopySlide {
    //Messages read from full qualified property file name and provide i18n capability.
    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.samples.messages");

    //Identify the entry point for the action. Returns a Value<String> because the return type is String.
    @Execute
    public Value<String> action(
            //Idx 1 would be displayed first, with a text box for entering the value.
            @Idx(index = "1", type = FILE)
            //UI labels.
            @Pkg(label = "[[CopySlide.inputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String inputFilePath,
            @Idx(index = "2", type = FILE)
            //UI labels.
            @Pkg(label = "[[CopySlide.outputFilePath.label]]")
            //Ensure that a validation error is thrown when the value is null.
            @FileExtension("PPTX")
            @NotEmpty
                    String  outputFilePath,
            @Idx(index = "3", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[CopySlide.CopyPosition.label]]", description = "[[CopySlide.CopyPosition.description]]")
            //Ensure that a validation error is thrown when the value is null.
            @NotEmpty
                    Double  copyPosition,
            @Idx(index = "4", type = RADIO,
                    //Providing the multiple options under the RADIO.
                    options = {
                            @Idx.Option(index = "4.1", pkg = @Pkg(node_label = "[[CopySlide.Append.node_label]]", label = "[[CopySlide.Append.label]]", value = "Append")),
                            @Idx.Option(index = "4.2", pkg = @Pkg(node_label = "[[CopySlide.Insert.node_label]]", label = "[[CopySlide.Insert.label]]", value = "Insert"))
                    })
            @Pkg(label = "[[CopySlide.CopyMode.label]]", default_value = "Append", default_value_type = DataType.STRING)
                    String copyMode,
            @Idx(index = "4.2.1", type = NUMBER)
            //UI labels.
            @Pkg(label = "[[CopySlide.InsertAt.label]]", description = "[[CopySlide.InsertAt.description]]")
            @NotEmpty
                    Double  insertPosition) {

        //Internal validation. No null check needed as we have NotEmpty property.
        if (copyMode.equals("Insert")){
            if (insertPosition <= 0)
                throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Insert At"));
            insertPosition-=1;
        }
        if (copyPosition <= 0)
            throw new BotCommandException(MESSAGES.getString("numberCannotBeLessThanOrEqualsZero", "Source Slide Position"));

        //Business logic
        copyPosition-=1;
        PPTOperations CopySlide_ToPPT = PPTLoaderClass.getInstance().getPPTInstance(inputFilePath,outputFilePath);
        Object result = CopySlide_ToPPT.CopySlide(copyMode, copyPosition, insertPosition);
        if (result instanceof Exception)
            //Return Exception
            throw new BotCommandException(MESSAGES.getString("customMessage", ((Exception) result).getMessage()));
        else
            //Return StringValue.
            return new StringValue(result);
    }
}
